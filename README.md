<details><summary>Задача</summary>
Нужно написать небольшой web сервис, который регистрирует клиента.
В качестве входной точки api метод, принимающий на вход документ вида 

json
{
  "name": "Смирнов Петр",
  "email": "test@yandex.ru",
  "password": "passw0rd"  
}

После создания клиента необходимо отправляем ему приветственное письмо.
</details>

## Requirements
 * php 7.4
 
## Installation
 - `git clone git@gitlab.com:consigliere-tests/symfony-register-test.git`
 - `composer install`
 - Configure db connection
 - `php bin/console doctrine:database:create`
 - `php bin/console doctrine:migrations:migrate`
 
 ## Testing
 - `APP_ENV=test php bin/console doctrine:database:create`
 - `APP_ENV=test php bin/console doctrine:migrations:migrate`
 - Перед каждым запуском тестов: `APP_ENV=test php bin/console doctrine:fixtures:load`
 - `php bin/phpunit`
 
 ## Using example
 ```
curl --location --request POST 'http://localhost/users/register' \
--header 'Accept: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
  "name": "Смирнов Петр",
  "email": "test211@test.ru",
  "password": "passw0rd"  
}'
```