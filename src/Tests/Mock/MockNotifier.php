<?php

declare(strict_types=1);

namespace App\Tests\Mock;

use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\Recipient;

class MockNotifier implements NotifierInterface
{
    /**
     * @var NotificationLog[]
     */
    protected $notifications = [];

    public function send(Notification $notification, Recipient ...$recipients): void
    {
        $this->notifications[] = new NotificationLog($notification, ...$recipients);
    }

    /**
     * @return NotificationLog[]
     */
    public function getNotifications(): array
    {
        return $this->notifications;
    }
}