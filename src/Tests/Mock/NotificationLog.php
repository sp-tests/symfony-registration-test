<?php

declare(strict_types=1);

namespace App\Tests\Mock;

use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\Recipient;

class NotificationLog
{
    private Notification $notification;
    /**
     * @var Recipient[]
     */
    private array $recipient;

    public function __construct(Notification $notification, Recipient ...$recipient)
    {
        $this->notification = $notification;
        $this->recipient = $recipient;
    }

    /**
     * @return Recipient[]
     */
    public function getRecipient(): array
    {
        return $this->recipient;
    }

    public function getNotification(): Notification
    {
        return $this->notification;
    }
}