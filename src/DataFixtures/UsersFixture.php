<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UsersFixture extends Fixture
{
    public const EXISTED_USER_EMAIL = 'test@test.te';

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail(static::EXISTED_USER_EMAIL);
        $user->setName('Василий');
        $user->setPassword(bin2hex(random_bytes(32)));
        $manager->persist($user);

        $manager->flush();
    }
}
