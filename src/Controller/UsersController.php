<?php

namespace App\Controller;

use App\DTO\RegistrationForm;
use App\Services\UserRegistrationService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Exception\ValidationFailedException;

class UsersController extends AbstractController
{
    public function __construct(LoggerInterface $logger)
    {
    }

    /**
     * @Route("/users/register", name="users", methods={"POST"})
     */
    public function register(Request $request, UserRegistrationService $registrationService): Response
    {
        $registrationForm = new RegistrationForm();
        $registrationForm->name = (string)$request->request->get('name');
        $registrationForm->email = (string)$request->request->get('email');
        $registrationForm->password = (string)$request->request->get('password');

        $user = $registrationService->register($registrationForm);

        return $this->json([
            'success' => true,
            'id' => $user->getId(),
        ]);
    }
}
