<?php

declare(strict_types=1);

namespace App\EventListener;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $exceptionEvent)
    {
        $exception = $exceptionEvent->getThrowable();
        $statusCode = $exception instanceof HttpException
            ? $exception->getStatusCode()
            : 500;
        $message = $exception->getMessage();
        $payload = [];

        switch (true) {
            case $exception instanceof ValidationFailedException:
                $statusCode = 400;
                $message = 'Invalid data';
                $payload = ['errors' => $this->formatValidationErrors($exception->getViolations())];

                break;
        }

        $jsonResponse = new JsonResponse(
            [
                'success' => false,
                'message' => $message,
                'data' => $payload,
            ],
            $statusCode
        );

        $exceptionEvent->setResponse($jsonResponse);
    }

    protected function formatValidationErrors(ConstraintViolationListInterface $violationList): array
    {
        $errors = [];

        /** @var ConstraintViolationInterface $violation */
        foreach ($violationList as $violation) {
            $property = $violation->getPropertyPath();

            if (!array_key_exists($property, $errors)) {
                $errors[$property] = [];
            }

            $errors[$property] = $violation->getMessage();
        }

        return $errors;
    }
}