<?php

declare(strict_types=1);

namespace App\EventListener;


use App\Entity\User;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\Recipient;

class UserRegisteredNotifier
{
    /**
     * @var NotifierInterface
     */
    private NotifierInterface $notifier;

    public function __construct(NotifierInterface $notifier)
    {
        $this->notifier = $notifier;
    }

    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        /** @var User $user */
        $user = $eventArgs->getObject();

        $notification = new Notification('User registered', ['email']);
        $recipient = new Recipient($user->getEmail());
        $this->notifier->send($notification, $recipient);
    }
}