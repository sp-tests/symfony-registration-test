<?php

declare(strict_types=1);

namespace App\Services;

use App\DTO\RegistrationForm;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserRegistrationService
{
    private EntityManagerInterface $entityManager;

    private PasswordEncoderInterface $passwordEncoder;

    private ValidatorInterface $validator;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        EntityManagerInterface $entityManager,
        EncoderFactoryInterface $passwordEncoderFactory,
        ValidatorInterface $validator,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoderFactory->getEncoder(User::class);
        $this->validator = $validator;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function register(RegistrationForm $registrationForm): User
    {
        $user = new User();
        $user->setEmail($registrationForm->email)
            ->setName($registrationForm->name)
            ->setPlainPassword($registrationForm->password);
        $validationErrors = $this->validator->validate($user);

        if ($validationErrors->count() > 0) {
            throw new ValidationFailedException('Validation failed', $validationErrors);
        }

        $passwordHash = $this->passwordEncoder->encodePassword($user->getPlainPassword(), $user->getSalt());
        $user->setPassword($passwordHash);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}