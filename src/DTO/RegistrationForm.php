<?php

declare(strict_types=1);

namespace App\DTO;

class RegistrationForm
{
    public string $name;
    public string $email;
    public string $password;
}