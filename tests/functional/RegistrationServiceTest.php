<?php

declare(strict_types=1);

use App\DataFixtures\UsersFixture;
use App\Entity\User;
use App\Tests\Mock\MockNotifier;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\MockObject\Stub\ReturnCallback;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;

class RegistrationServiceTest extends WebTestCase
{
    private ?EntityManager $entityManager;
    private KernelBrowser $client;

    public function testUserCantBeRegisteredOnBusyEmail(): void
    {
        $this->registerUser(UsersFixture::EXISTED_USER_EMAIL);
        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testUserSuccessfullyRegistered(): void
    {
        $email = 'free@email.com';

        $this->registerUser($email);
        $response = $this->client->getResponse();
        $responseBody = json_decode($response->getContent());
        /** @var User $user */
        $user = $this->entityManager
            ->getRepository(User::class)
            ->findOneBy(['email' => $email]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertNotNull($user);
        $this->assertEquals($user->getId(), $responseBody->id ?? null);
    }

    public function testUserNotifiedAfterRegistration(): void
    {
        $email = 'free2@email.com';
        $this->client->enableProfiler();

        /** @var MockNotifier $notifier */
        $notifier = self::$kernel->getContainer()->get(NotifierInterface::class);

        $this->registerUser($email);

        $lastNotification = $notifier->getNotifications()[0] ?? null;
        $recipientEmail = $lastNotification->getRecipient()[0]->getEmail() ?? null;

        $this->assertNotNull($lastNotification);
        $this->assertEquals($recipientEmail, $email);
    }

    protected function registerUser(string $email): void
    {
        $request = [
            "name" => "Смирнов Петр",
            "email" => $email,
            "password" => "passw0rd",
        ];

        $this->client->request(
            'POST',
            '/users/register',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
            ],
            json_encode($request)
        );
    }

    protected function setUp(): void
    {
        $kernel = self::bootKernel();


        $this->client = $kernel->getContainer()->get('test.client');
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}